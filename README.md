# Procedural Generation 

# Introduction
The animal spawner has a few options to make animal population a bit easier on a developer or artist.


# Package Includes

 - Sample scene (you can play around with the editor).
  
 - 1 Script with comments.
        
 - 5 animal prefabs.
             
 - 5 rough animal models.
              
 - 1 Terrain.
     
 - 1 Basic shader.

# Instructions
 - Import the package.
   
 - Go to Window.
         
 - Then click on the animal spawner.
             
 - A window should open where you can then edit the attributes.

# Uses
 You can control the number of animals you spawn.
 You control what type of animal will spawn.
 You can change the color of the animals you spawn.
 You can change the area size in which the animals will spawn.

# Tutorials used
I used brackeys editor scripting tutorial to show me how to get the color picker.
The rest i made using various entries on both unity forums and scripting API.


