﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AnimalSpawns : EditorWindow {

    //List of items in the animal type dropdown menu.
    public string[] Options = new string[] { "Cow", "Pig", "Sheep", "Duck", "Chicken" };

    //The index of options in the animal type dropdown meny.
    public int index = 0;

    //The string of the amount of animals to spawn that gets entered by user.
    string myString = "0";

    //The string of x axis spawn area width that gets entered by user.
    string _xArea = "10";

    //The string of y axis spawn area height that gets entered by user.
    string _yArea = "0";

    //The string of z axis spawn area width that gets entered by user.
    string _zArea = "10";

    //Default values for x, y, and z axis.
    public int _SpawnAreaX = 10;
    public int _SpawnAreaY = 0;
    public int _SpawnAreaZ = 10;

    //Value for the amount of objects to spawn.
    private int _NumberOfSpawns;

    //reference to the color we're changing on the objects.
    Color color;

    //This is used to correct the prefab rotations.
    Quaternion correctRotation = Quaternion.identity;

    //Making a new menu item in the window menu.
    [MenuItem ("Window/AnimalSpawner")]

    //Spawning a new editor window for the animal spawner.
    public static void ShowWindow () {
        GetWindow<AnimalSpawns> ("AnimalSpawner");
    }

    //The layout of the editor window.
    void OnGUI () {

        //Just a label
        GUILayout.Label ("Animal Manager", EditorStyles.boldLabel);

        //Space between the label and its section.
        GUILayout.Space (5f);

        //This is the string and input for the amount of spawns.
        myString = EditorGUILayout.TextField ("Amount Of Spawns", myString);

        //This is the dropdown menu
        index = EditorGUILayout.Popup ("Type Of Animal", index, Options);

        //Space between sections
        GUILayout.Space (10f);

        //Just a label
        GUILayout.Label ("Spawn Area Manager", EditorStyles.boldLabel);

        //Space between the label and its section.
        GUILayout.Space (5f);

        //This is the text input fields for the axis spawn sizes.
        _xArea = EditorGUILayout.TextField ("X Axis Width", _xArea);
        _yArea = EditorGUILayout.TextField ("Y Axis Height", _yArea);
        _zArea = EditorGUILayout.TextField ("Z Axis Width", _zArea);

        //This button creates the prefabs as well as updates the variables the user enters in.
        if (GUILayout.Button ("Create")) {
            _NumberOfSpawns = int.Parse (myString.ToString ());
            _SpawnAreaX = int.Parse (_xArea.ToString ());
            _SpawnAreaY = int.Parse (_yArea.ToString ());
            _SpawnAreaZ = int.Parse (_zArea.ToString ());
            correctRotation.eulerAngles = new Vector3 (-90, 0, 0);
            SpawnTheAnimal ();
        }

        //Space between sections.
        GUILayout.Space (10f);

        //Just a label.
        GUILayout.Label ("Change Colour Of Selected Object", EditorStyles.boldLabel);

        //Space between the label and its section.
        GUILayout.Space (5f);

        //This is used to make a color field selector in the editor window.
        color = EditorGUILayout.ColorField ("Color", color);

        //This button is udes to change the color of objects.
        if (GUILayout.Button ("Change Color")) {
            ColorChange ();
        }
    }

    //This function changes the colors of the selected objects.
    void ColorChange () {
        foreach (GameObject obj in Selection.gameObjects) {
            Renderer renderer = obj.GetComponent<Renderer> ();
            if (renderer != null) {
                renderer.sharedMaterial.color = color;
            }
        }

    }

    //These are the details of the dropdown menu.
    //Each case is a different option.
    //And within that is then what should happen once that option is picked.
    void SpawnTheAnimal () {
        switch (index) {
            case 0:
                for (int i = 0; i < _NumberOfSpawns; i++) {
                    Vector3 _RandomPosition = new Vector3 (Random.Range (-_SpawnAreaX, _SpawnAreaX), Random.Range (-_SpawnAreaY, _SpawnAreaY), Random.Range (-_SpawnAreaZ, _SpawnAreaZ));
                    Instantiate (Resources.Load ("Cow"), _RandomPosition, correctRotation);
                }
                break;
            case 1:
                for (int i = 0; i < _NumberOfSpawns; i++) {
                    Vector3 _RandomPosition = new Vector3 (Random.Range (-_SpawnAreaX, _SpawnAreaX), Random.Range (-_SpawnAreaY, _SpawnAreaY), Random.Range (-_SpawnAreaZ, _SpawnAreaZ));
                    Instantiate (Resources.Load ("Pig"), _RandomPosition, correctRotation);
                }
                break;
            case 2:
                for (int i = 0; i < _NumberOfSpawns; i++) {
                    Vector3 _RandomPosition = new Vector3 (Random.Range (-_SpawnAreaX, _SpawnAreaX), Random.Range (-_SpawnAreaY, _SpawnAreaY), Random.Range (-_SpawnAreaZ, _SpawnAreaZ));
                    Instantiate (Resources.Load ("Sheep"), _RandomPosition, correctRotation);
                }
                break;
            case 3:
                for (int i = 0; i < _NumberOfSpawns; i++) {
                    Vector3 _RandomPosition = new Vector3 (Random.Range (-_SpawnAreaX, _SpawnAreaX), Random.Range (-_SpawnAreaY, _SpawnAreaY), Random.Range (-_SpawnAreaZ, _SpawnAreaZ));
                    Instantiate (Resources.Load ("Duck"), _RandomPosition, correctRotation);
                }
                break;
            case 4:
                for (int i = 0; i < _NumberOfSpawns; i++) {
                    Vector3 _RandomPosition = new Vector3 (Random.Range (-_SpawnAreaX, _SpawnAreaX), Random.Range (-_SpawnAreaY, _SpawnAreaY), Random.Range (-_SpawnAreaZ, _SpawnAreaZ));
                    Instantiate (Resources.Load ("Chicken"), _RandomPosition, correctRotation);
                }
                break;
            default:
                Debug.LogError ("Unrecognized Option");
                break;
        }
    }
}